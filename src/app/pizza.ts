export class Pizza {
  id: number;
  price: number;
  name: string;
  description: string;
}
