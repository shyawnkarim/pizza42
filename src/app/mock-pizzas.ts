import { Pizza } from './pizza';

export const PIZZAS: Pizza[] = [
  { price: 11, name: 'Cheese', id: 0, description: 'cheesy' },
  { price: 12, name: 'Pepperoni', id: 1, description: 'greasy' },
  { price: 13, name: 'Hawaiian', id: 2, description: 'tropical' },
  { price: 14, name: 'Vegetarian', id: 3, description: 'farm to table' },
  { price: 15, name: 'Margarita', id: 4, description: 'classic' },
  { price: 16, name: 'BBQ Chicken', id: 5, description: 'hot off the grill' },
  { price: 17, name: 'Italian', id: 6, description: 'spicy' },
  { price: 18, name: 'Supreme', id: 7, description: 'good mix of veggies and meats' },
  { price: 19, name: 'Meats', id: 8, description: 'meat, piled high' },
  { price: 20, name: 'The 42', id: 9, description: 'everything including the kitchen sink' }
];
