import { Injectable } from '@angular/core';
import { Pizza } from './pizza';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  cart: Pizza[];
  totalPrice: number;
  count: number;

  constructor() {
    this.cart = [];
    this.totalPrice = 0;
    this.count = 0;
  }

  addToCart(pizza: Pizza): void {
    this.cart.push(pizza);
    this.totalPrice += pizza.price;
    this.count++;
  }

  removeFromCart(pizza: Pizza): void {
    let index: number = -1;

    this.cart.some(function (value: Pizza, i: number): boolean {
      if (value.id !== pizza.id) {
        return false;
      }

      index = i;
      return true;
    });

    if (index !== -1) {
      this.cart.splice(index, 1);
      this.totalPrice -= pizza.price;
      this.count--;
    }
  }

  isEmpty(): boolean {
    return this.cart.length === 0;
  }
}
