import { Routes } from '@angular/router';
import { PizzasComponent } from './pizzas/pizzas.component';
import { CallbackComponent } from './callback/callback.component';

export const ROUTES: Routes = [
  { path: '', component: PizzasComponent },
  { path: 'callback', component: CallbackComponent },
  { path: '**', redirectTo: '' }
];
