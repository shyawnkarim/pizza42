import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Pizza } from './pizza';
import { PIZZAS } from './mock-pizzas';

@Injectable({
  providedIn: 'root'
})
export class PizzaService {

  constructor() {

  }

  getPizzas(): Observable<Pizza[]> {
    return of(PIZZAS);
  }
}
