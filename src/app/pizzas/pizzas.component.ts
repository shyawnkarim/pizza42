import {Component, OnInit} from '@angular/core';

import {Pizza} from '../pizza';
import {PizzaService} from '../pizza.service';
import {CartService} from '../cart.service';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-pizzas',
  templateUrl: './pizzas.component.html',
  styleUrls: ['./pizzas.component.css']
})
export class PizzasComponent implements OnInit {

  selectedPizza: Pizza;

  pizzas: Pizza[];

  constructor(private pizzaService: PizzaService, private cartService: CartService, public auth: AuthService) {
  }

  ngOnInit() {
    this.getPizzas();
  }

  onSelect(pizza: Pizza): void {
    this.selectedPizza = pizza;
  }

  addToCart(pizza: Pizza): void {
    if (!this.auth.isAuthenticated()) {
      alert('Log in to add pizza to cart');
      return this.auth.login();
    }
    this.cartService.addToCart(pizza);
  }

  getPizzas(): void {
    this.pizzaService.getPizzas()
      .subscribe(pizzas => this.pizzas = pizzas);
  }

}
