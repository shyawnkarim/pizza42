interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: 'qwMNckpwPXNN-RebbqvZaCubu7Lcx7sI',
  domain: 'skarim.auth0.com',
  callbackURL: 'http://skarim-pizza42.s3-website-us-west-2.amazonaws.com/callback'
};
