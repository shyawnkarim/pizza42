# Pizza42

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.5.

## AWS Hosting
http://skarim-pizza42.s3-website-us-west-2.amazonaws.com

## Instructions
* Select a pizza.
* See pizza details.
* Select add to cart.
* Sign in if not already signed in.
* Continue to add items to the cart or remove them using the remove button in the cart.


